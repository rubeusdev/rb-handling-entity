<?php
namespace Rubeus\ManipulacaoEntidade\Aplicacao;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade as ConteinerEntidade;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class Cadastro{
    
    public function cadastrar($mensagem,$preFixo=''){
        $validarEntidade = new ValidarDadosEntidade();
        
        $validarEntidade->setPreFixo($preFixo);
        
        $validarEntidade->setEntidade(ConteinerEntidade::getInstancia($mensagem->getCampo("entidade")->get('valor')));
        
        if(!$validarEntidade->vaidarCampo($mensagem)){
            $validarEntidade->returnErro($mensagem);
            return;
        }
        
        $repositorio = Conteiner::getInstancia('Repositorio');
        
        if($repositorio->registrarPadrao($validarEntidade->getEntidade())){
            $validarEntidade->finalizar($mensagem);
        }
        else{
            $validarEntidade->finalizarErro($mensagem,'req_fal'); 
        }
        $resultado = $mensagem->getResultadoEtapa($mensagem->getEtapaAtual());
        return $resultado['success'];
    }
    
}