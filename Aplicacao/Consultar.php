<?php
namespace Rubeus\ManipulacaoEntidade\Aplicacao;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;
use Rubeus\ManipulacaoEntidade\Dominio\ConteinerEntidade as ConteinerEntidade;

class Consultar{
   
    public function consultar($mensagem){
        if(!$mensagem->getCampo("entidade")->get('valor')){
            $mensagem->getCampo("entidade")->set('valor', $mensagem->getProcesso());
        }
        $repositorio = Conteiner::getInstancia('RepositorioAuxilixar');
        
        $entidade = ConteinerEntidade::getInstancia($mensagem->getCampo("entidade")->get('valor'));
        
        $dados = $repositorio->consultaReduzida($entidade);
        
        if($dados){
            $mensagem->setResultadoEtapa(true, false,array('dados' => $dados));
        }else{
            $mensagem->setResultadoEtapa(false, false);
        }
    }
}