<?php
namespace Rubeus\ManipulacaoEntidade\Aplicacao;
use Inedu\Gerenciador\ManipulacaoEntidade\Dominio\ConteinerEntidade as ConteinerEntidade;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class Deletar{
    
    public function deletar($mensagem){
        $entidade = ConteinerEntidade::getInstancia($mensagem->getCampo("entidade")->get('valor'));
        
        $campo = Conteiner::getInstancia('Campo');
        $id = $campo->criar($mensagem->getCampo($entidade->getEntidade().'::id'));
        
        if($campo->getErro()){
            $mensagem->setResultadoEtapa(false, $campo->getErro());
            return;
        }
        
        $entidade->setId($id); 
        $repositorio = Conteiner::getInstancia('Repositorio');
        
        if($repositorio->remover($entidade))
            $mensagem->setResultadoEtapa(true, false);
        else
            $mensagem->setResultadoEtapa(false, 'req_fal');
        
    }
    
}