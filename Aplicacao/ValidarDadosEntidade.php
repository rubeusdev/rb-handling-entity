<?php
namespace Rubeus\ManipulacaoEntidade\Aplicacao;
use Rubeus\ContenerDependencia\Conteiner as Conteiner;

class ValidarDadosEntidade{
    private $entidade;
    private $dados;
    private $erro;
    private $preFixo;
    
    public function __construct($entidade=false) {
        $this->campo = Conteiner::getInstancia('Campo');
        $this->entidade = $entidade;
        $this->erro = array();
        $this->preFixo = '';
    }
    
    public function setPreFixo($preFixo){
        $this->preFixo = $preFixo;
    } 
    
    public function getErro(){
        return $this->erro;
    }
    
    public function getEntidade(){
        return $this->entidade;
    }
    
    public function setEntidade($entidade){
        $this->entidade = $entidade;
    }
    
    public function adicionarDados($chave, $valor){
        $this->dados[$chave] = $valor;
    }
    
    public function adicionarErro($campo, $erro){
        $this->erro[] = array('campo' => $campo, 'erro' => $erro);
    }
    
    public function vaidarCampo($mensagem){
        $campos = $this->entidade->getCampos();
        $entidade = $this->preFixo.$this->entidade->getEntidade();
       
        foreach($campos as $campo){            
            $dadosCampo = $mensagem->getCampo($entidade.'::'.$campo);
            
            if(!$dadosCampo)continue;
            
            $valor = $this->campo->criar($dadosCampo);
            $valor = $mensagem->getCampo($entidade.'::'.$campo)->get('foiEnviado')&&$valor===false?null:$valor;
            
            if($this->campo->getErro())
                $this->adicionarErro($mensagem->getCampo($entidade.'::'.$campo)->get('nome'), $this->campo->getErro());
            else{
                $this->entidade->set($campo, $valor);
            }
            
        }
        return empty($this->erro);
    }
    
    public function finalizarErro($mensagem,$msg=false){  
        $mensagem->setResultadoEtapa(false, $msg,false,2);
    } 
    
    private function getValor($array){
        return count($array) > 1 ? $array : $array[0];
    }
    
    public function finalizar($mensagem,$msg=false,$array=false){
        $id = array();
        $id[] = $this->entidade->getId();
        $this->entidade->percorrer(1);
        if($this->entidade->getId() && !is_null($this->entidade->getId()) && $this->entidade->getId() != $id[0]){
            $id[] = $this->entidade->getId();
            while($this->entidade->percorrer()){
                $id[] = $this->entidade->getId();
            }
        }
        if(!$array)$array= array('id'=> $this->getValor($id));
        $mensagem->setCampo($this->entidade->getClasseMap().'::id',$array['id']);
        
        $mensagem->setResultadoEtapa(true, $msg, $array);
    } 
    
    public function returnErro($mensagem){
       $mensagem->setResultadoEtapa(false, false,array('erro' => $this->erro)); 
    }
    
}