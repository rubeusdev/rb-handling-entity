<?php
namespace Rubeus\ManipulacaoEntidade\Dominio;
use Rubeus\Servicos\Json\Json;

abstract class ConteinerEntidade{
    private static $pool;
    private static $poolInstancia;
                  
    public static function registrar($entidade,$valor){
        self::$poolInstancia[$entidade] = $valor;    
    }

    public static function getInstancia($entidade){
        if (isset(self::$poolInstancia[$entidade])) {
            return  self::$poolInstancia[$entidade];
        }      
        if(is_null(self::$pool)) self::$pool = Json::lerArq(DIR_BASE.'/src/entidades/Entidades.json');  
        $entidade = self::$pool->$entidade;
        return new $entidade();
    }
   
}
