<?php
namespace Rubeus\ManipulacaoEntidade\Infra\Repositorio;

class Repositorio{
    private $entidades = array();
    private $remover = array();
    
    public function add($chave,$entidade){
        $this->entidades[$chave] = $entidade;
    }
    
    public function rmv($chave){
        $this->remover[] = $this->entidades[$chave];
        unset($this->entidades[$chave]);
    }
    
    public function persistir(){
        foreach($this->entidades as $entidade)
            $entidade->salvar();
        
        foreach($this->remover as $entidade)
            $entidade->delete();
        
    }
    
    public function registrarPadrao($entidade){
        $qtd = $entidade->getQtdData();
        $ativo=array();
        $momento=array();
        if($qtd>0){
            for($i=0;$i<$qtd;$i++){
                $ativo[]=1;
                $momento[]=date('Y-m-d H:i:s');
            }
        }else{
            $ativo=1;
            $momento=date('Y-m-d H:i:s');
        }
        
        if(!$entidade->getId()){
            $entidade->set('ativo',$ativo);
            if (!$entidade->get('momento')) {
                $entidade->set('momento',$momento);
            }
        }
        return $this->persistirEntidade($entidade);
    }
    
    public function popular($dados, $entidade){
        foreach($dados as $key => $valor)
            $entidade->__set($key, $valor);
    }
    
    public function persistirEntidade($entidade, $update=false){
        $entidade->salvar($update);
//        var_dump($entidade->getQtdErro(), $entidade->getErro());
        return ! $entidade->getQtdErro();
    }
    
    public function cadastrar($dados, $entidade,$update=false){
        foreach($dados as $key => $valor)
            $entidade->__set($key, $valor);
        $entidade->salvar($update);
    }
    
    public function remover($entidade){
        $entidade->deletar();
        return ! $entidade->getQtdErro();
    }
    
    public function consultarId($entidade){
        return $entidade->carregar('id');
    }
    
    public function carregar($entidade){
        return $entidade->carregar();
    }
    
    public function converteSimNao($key,&$retorno,&$dados){
        if($dados)
            $retorno[$key] = 2;
        else $retorno[$key] = 1;
    }
}