<?php
namespace Rubeus\ManipulacaoEntidade\Infra\Repositorio;


class RepositorioEntidadeAuxiliar extends Repositorio{
   
    public function consultaReduzida($entidade, $nome=false){
        if($nome)$entidade->filtro()->like('titulo',$nome)->add('and','ativo = 1');            
        else $entidade->setAtivo(1);
        return $entidade->carregar('id, titulo',false);
    }
    
    public function consulta($entidade,$nome=false){
        if($nome)$entidade->filtro()->like('titulo',$nome)->add('and','ativo = 1');            
        else $entidade->setAtivo(1);
        return $entidade->carregar('id, titulo, descricao',false);
    }
    
}