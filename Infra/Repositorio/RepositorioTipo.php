<?php
namespace Rubeus\ManipulacaoEntidade\Infra\Repositorio;

class RepositorioTipo extends Repositorio{
   
    public function getEntidade($tipo){
        return $tipo->carregar('entidade',false,'{entidade}');
    }
    
    public function getEntidades($tipo){
        return $tipo->carregar('entidade',false);
    }
}